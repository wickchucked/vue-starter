using System.Collections.Generic;
using vue_starter.Models;

namespace vue_starter.Providers
{
    public interface IWeatherProvider
    {
        List<WeatherForecast> GetForecasts();
    }
}
