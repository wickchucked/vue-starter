import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui';
import router from './router/index'
import store from './store'
import { sync } from 'vuex-router-sync'
import App from 'components/app-root'
import { FontAwesomeIcon } from './icons'
import locale from 'element-ui/lib/locale/lang/en'


// Registration of global components
Vue.component('icon', FontAwesomeIcon)
Vue.use(ElementUI, {locale});

Vue.prototype.$http = axios

sync(store, router)

const app = new Vue({
  store,
  router,
  ...App
})

export {
  app,
  router,
  store
}
